<TS language="hi_IN" version="2.1">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>नया पता बनाएँ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Show QR code for the currently selected address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Show QR code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Delete the currently selected address from the list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Export the data in the current tab to a file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>C&amp;lose</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+54"/>
        <source>Choose the address to send coins to</source>
        <translation>सिक्कों को भेजने के लिए पता चुनें</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose the address to receive coins with</source>
        <translation>सिक्कों को प्राप्त करने के लिए पता चुनें</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>C&amp;hoose</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Sending addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Receiving addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>These are your Ion addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>These are your Ion addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Copy Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy &amp;Label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Show address QR code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+148"/>
        <source>QR code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+66"/>
        <source>Export Address List</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Comma separated file (*.csv)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Exporting Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+171"/>
        <source>Label</source>
        <translation>लेबल</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>पता</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>(no label)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Encrypt wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+8"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-3"/>
        <source>Unlock wallet for mixing only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Unlock wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+44"/>
        <source>Confirm wallet encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR ION&lt;/b&gt;!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <location line="+60"/>
        <source>Wallet encrypted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-70"/>
        <location line="+12"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your funds from being stolen by malware infecting your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-8"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. Previous backups of the unencrypted wallet file contain the same HD seed and still have full access to all your funds just like the new, encrypted wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons, previous backups of the unencrypted wallet file will become useless as soon as you start using the new, encrypted wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <location line="+7"/>
        <location line="+43"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+49"/>
        <source>The supplied passphrases do not match.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-37"/>
        <source>Wallet unlock failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <location line="+24"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+89"/>
        <source>IP/Netmask</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../ion.cpp" line="+551"/>
        <source>A fatal error occurred. Ion Core can no longer continue safely and will quit.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../bitcoingui.cpp" line="+1119"/>
        <source>Ion Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-976"/>
        <source>Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Node</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Overview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show general overview of wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Send</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Send coins to a Ion address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Receive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Request payments (generates QR codes and ion: URIs)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Show information about Ion Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-7"/>
        <source>&amp;About %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Modify configuration options for %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Show / Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Backup Wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Backup wallet to another location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Change Passphrase...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Unlock Wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unlock wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Lock Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sign &amp;message...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sign messages with your Ion addresses to prove you own them</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Verify message...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Verify messages to ensure they were signed with specified Ion addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show diagnostic information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Debug console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Network Monitor</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show network monitor</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Peers list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show peers info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet &amp;Repair</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show wallet repair options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Open Wallet &amp;Configuration File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Open configuration file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Automatic &amp;Backups</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show automatically created wallet backups</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Sending addresses...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Receiving addresses...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;URI...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Open a ion: URI or payment request</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show the %1 help message to get a list with possible Ion command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+288"/>
        <source>%1 client</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-286"/>
        <source>&amp;PrivateSend information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show the PrivateSend basic information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Tabs toolbar</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+366"/>
        <source>%n active connection(s) to Ion network</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Network activity disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Syncing Headers (%1%)...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Synchronizing with network...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Indexing blocks on disk...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Processing blocks on disk...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Reindexing blocks on disk...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Connecting to peers...</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>%1 behind</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Catching up...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Last received block was generated %1 ago.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Up to date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Synchronizing additional data: %p%</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning</source>
        <translation>चेतावनी</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Information</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Date: %1
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Type: %1
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sent transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt; for mixing only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ClientModel</name>
    <message>
        <location filename="../clientmodel.cpp" line="+203"/>
        <source>Network Alert</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+48"/>
        <source>Quantity:</source>
        <translation>मात्रा:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>राशि:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Fee:</source>
        <translation>फ़ीस:</translation>
    </message>
    <message>
        <location line="-188"/>
        <source>Coin Selection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+140"/>
        <source>Dust:</source>
        <translation>धूल:</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>After Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>toggle lock state</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>(1 locked)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>PS Rounds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>मिति</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+62"/>
        <source>Copy address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+26"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-25"/>
        <source>Copy transaction ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Lock unspent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unlock unspent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+148"/>
        <source>Please switch to &quot;List mode&quot; to use this function.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+196"/>
        <source>Non-anonymized input selected. &lt;b&gt;PrivateSend will be disabled.&lt;/b&gt;&lt;br&gt;&lt;br&gt;If you still want to use PrivateSend, please deselect all non-anonymized inputs first and then check the PrivateSend checkbox again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>(%1 locked)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+159"/>
        <source>yes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>This label turns red if any recipient receives an amount smaller than the current dust threshold.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Can vary +/- %1 duff(s) per input.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+43"/>
        <location line="+56"/>
        <source>(no label)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>n/a</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+29"/>
        <source>New receiving address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+71"/>
        <source>The entered address &quot;%1&quot; is not a valid Ion address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not unlock wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>New key generation failed.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+79"/>
        <source>A new data directory will be created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Path already exists, and is not a directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot create data directory here.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+41"/>
        <source>version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Usage:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>UI Options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Choose data directory on startup (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set language, for example &quot;de_DE&quot; (default: system locale)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Start minimized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set SSL root certificates for payment request (default: -system-)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show splash screen on startup (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset all settings changed in the GUI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+42"/>
        <source>PrivateSend information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h3&gt;PrivateSend Basics&lt;/h3&gt; PrivateSend gives you true financial privacy by obscuring the origins of your funds. All the Ion in your wallet is comprised of different &quot;inputs&quot; which you can think of as separate, discrete coins.&lt;br&gt; PrivateSend uses an innovative process to mix your inputs with the inputs of two other people, without having your coins ever leave your wallet. You retain control of your money at all times.&lt;hr&gt; &lt;b&gt;The PrivateSend process works like this:&lt;/b&gt;&lt;ol type=&quot;1&quot;&gt; &lt;li&gt;PrivateSend begins by breaking your transaction inputs down into standard denominations. These denominations are 0.001 ION, 0.01 ION, 0.1 ION, 1 ION and 10 ION -- sort of like the paper money you use every day.&lt;/li&gt; &lt;li&gt;Your wallet then sends requests to specially configured software nodes on the network, called &quot;masternodes.&quot; These masternodes are informed then that you are interested in mixing a certain denomination. No identifiable information is sent to the masternodes, so they never know &quot;who&quot; you are.&lt;/li&gt; &lt;li&gt;When two other people send similar messages, indicating that they wish to mix the same denomination, a mixing session begins. The masternode mixes up the inputs and instructs all three users&apos; wallets to pay the now-transformed input back to themselves. Your wallet pays that denomination directly to itself, but in a different address (called a change address).&lt;/li&gt; &lt;li&gt;In order to fully obscure your funds, your wallet must repeat this process a number of times with each denomination. Each time the process is completed, it&apos;s called a &quot;round.&quot; Each round of PrivateSend makes it exponentially more difficult to determine where your funds originated.&lt;/li&gt; &lt;li&gt;This mixing process happens in the background without any intervention on your part. When you wish to make a transaction, your funds will already be anonymized. No additional waiting is required.&lt;/li&gt; &lt;/ol&gt; &lt;hr&gt;&lt;b&gt;IMPORTANT:&lt;/b&gt; Your wallet only contains 1000 of these &quot;change addresses.&quot; Every time a mixing event happens, up to 9 of your addresses are used up. This means those 1000 addresses last for about 100 mixing events. When 900 of them are used, your wallet must create more addresses. It can only do this, however, if you have automatic backups enabled.&lt;br&gt; Consequently, users who have backups disabled will also have PrivateSend disabled. &lt;hr&gt;For more information, see the &lt;a href=&quot;https://docs.ionomy.com/en/latest/wallets/ioncoin/privatesend-instantsend.html&quot;&gt;PrivateSend documentation&lt;/a&gt;.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+157"/>
        <source>When you click OK, %1 will begin to download and process the full %4 block chain (%2GB) starting with the earliest transactions in %3 when %4 initially launched.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>This initial synchronisation is very demanding, and may expose hardware problems with your computer that had previously gone unnoticed. Each time you run %1, it will continue downloading where it left off.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>If you have chosen to limit block chain storage (pruning), the historical data must still be downloaded and processed, but will be deleted afterward to keep your disk usage low.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-160"/>
        <source>Use the default data directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../intro.cpp" line="+26"/>
        <source>At least %1 GB of data will be stored in this directory, and it will grow over time.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Approximately %1 GB of data will be stored in this directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>%1 will download and store a copy of the Ion block chain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>The wallet will also be stored in this directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+73"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 GB of free space available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>(of %1 GB needed)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MasternodeList</name>
    <message>
        <location filename="../forms/masternodelist.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+123"/>
        <source>Address</source>
        <translation>पता</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Payee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-58"/>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-44"/>
        <source>Filter List:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Filter masternode list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Node Count:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-48"/>
        <source>DIP3 Masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Show only masternodes this wallet has keys for.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>My masternodes only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+65"/>
        <source>PoSe Score</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Registered</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Last Paid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Next Payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Operator Reward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../masternodelist.cpp" line="+67"/>
        <source>Copy ProTx Hash</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Collateral Outpoint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+106"/>
        <source>ENABLED</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>POSE_BANNED</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+4"/>
        <location line="+7"/>
        <source>UNKNOWN</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>to UNKNOWN</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>but not claimed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>NONE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+87"/>
        <source>Additional information for DIP3 Masternode %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+117"/>
        <source>Recent transactions may not yet be visible, and therefore your wallet&apos;s balance might be incorrect. This information will be correct once your wallet has finished synchronizing with the Ion network, as detailed below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Attempting to spend Ion that are affected by not-yet-displayed transactions will not be accepted by the network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+63"/>
        <source>Number of blocks left</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <location filename="../modaloverlay.cpp" line="+141"/>
        <source>Unknown...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>Progress increase per hour</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="-1"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+48"/>
        <source>Select payment request file to open</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Main</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Size of &amp;database cache</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>MB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Number of script &amp;verification threads</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+165"/>
        <source>Amount of Ion to keep anonymized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-129"/>
        <source>W&amp;allet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Show additional tab listing all your masternodes in first sub-tab&lt;br/&gt;and all masternodes on the network in second sub-tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Masternodes Tab</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Show additional information and buttons for PrivateSend on overview screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable advanced PrivateSend interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Show warning dialog when PrivateSend detects that wallet has very low number of keys left.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warn if PrivateSend is running out of keys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Whether to use experimental PrivateSend mode with multiple mixing sessions per block.&lt;br/&gt;Note: You must use this feature carefully.&lt;br/&gt;Make sure you always have recent wallet (auto)backup in a safe place!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable PrivateSend &amp;multi-session</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction&lt;br/&gt;cannot be used until that transaction has at least one confirmation.&lt;br/&gt;This also affects how your balance is computed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>PrivateSend rounds to use</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>This amount acts as a threshold to turn off PrivateSend once it&apos;s reached.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+60"/>
        <source>Automatically open the Ion Core client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Accept connections from outside</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow incoming connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to the Ion network through a SOCKS5 proxy.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+315"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+144"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items.&lt;br/&gt;%s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-663"/>
        <source>Expert</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+81"/>
        <source>This setting determines the amount of individual masternodes that an input will be anonymized through.&lt;br/&gt;More rounds of anonymization gives a higher degree of privacy, but also costs more in fees.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-75"/>
        <source>Whether to show coin control features or not.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-116"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+116"/>
        <source>Enable coin &amp;control features</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Show system popups for PrivateSend mixing transactions&lt;br/&gt;just like for all other transaction types.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show popups for PrivateSend transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+93"/>
        <source>&amp;Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Map port using &amp;UPnP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-180"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows, if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-36"/>
        <source>IPv4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Ion network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Hide the icon from the system tray.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide tray icon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>Language missing or translation incomplete? Help contributing translations here:
https://www.transifex.com/ioncoincore/ioncore/</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>User Interface Theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Decimal digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Third party transaction URLs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>Active command-line options that override above options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+110"/>
        <source>default</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+64"/>
        <source>none</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+85"/>
        <source>Confirm options reset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+48"/>
        <source>Client restart required to activate changes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-48"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>This change would require a client restart.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>The supplied proxy address is invalid.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+20"/>
        <source>Form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+53"/>
        <location line="+355"/>
        <location line="+258"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Ion network after a connection is established, but this process has not completed yet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-364"/>
        <source>Available:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-160"/>
        <source>Balances</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+42"/>
        <source>Total:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Current total balance in watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Watch-only:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+51"/>
        <source>Your current balance in watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <location filename="../overviewpage.cpp" line="+511"/>
        <location line="+12"/>
        <location line="+8"/>
        <location line="+55"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+14"/>
        <source>PrivateSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+49"/>
        <source>Status:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Enabled/Disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Completion:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+109"/>
        <source>Try to manually submit a PrivateSend request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Reset the current status of PrivateSend (can interrupt PrivateSend if it&apos;s in the process of Mixing, which can cost you money!)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Information about PrivateSend and Mixing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-107"/>
        <source>Amount and Rounds:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>0 ION / 0 Rounds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Submitted Denom:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>n/a</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+131"/>
        <source>Recent transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-101"/>
        <source>Start/Stop Mixing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-74"/>
        <source>PrivateSend Balance:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>The denominations you submitted to the Masternode.&lt;br&gt;To mix, other users must submit the exact same denominations.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>(Last Message)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Try Mix</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Reset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../overviewpage.cpp" line="-475"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>out of sync</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Automatic backups are disabled, no mixing available!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <location line="+318"/>
        <location line="+149"/>
        <source>Start Mixing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-465"/>
        <location line="+468"/>
        <source>Stop Mixing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-289"/>
        <location line="+6"/>
        <source>No inputs detected</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="-2"/>
        <location line="+20"/>
        <location line="+10"/>
        <source>%n Rounds</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Found enough compatible inputs to anonymize %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Not enough compatible inputs to anonymize &lt;span style=&apos;color:red;&apos;&gt;%1&lt;/span&gt;,&lt;br&gt;will anonymize &lt;span style=&apos;color:red;&apos;&gt;%2&lt;/span&gt; instead</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+58"/>
        <source>Overall progress</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denominated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Mixed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Anonymized</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>Denominated inputs have %5 of %n rounds on average</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>keys left: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <location line="+48"/>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+3"/>
        <source>Disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-165"/>
        <source>Very low number of keys left since last automatic backup!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>We are about to create a new automatic backup for you, however &lt;span style=&apos;color:red;&apos;&gt; you should always make sure you have backups saved in some safe place&lt;/span&gt;!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Note: You can turn this message off in options.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>WARNING! Something went wrong on automatic backup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <location line="+16"/>
        <source>ERROR! Failed to create automatic backup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-15"/>
        <location line="+17"/>
        <source>Mixing is disabled, please close your wallet and fix the issue!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-11"/>
        <source>Enabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>see debug.log for details.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>WARNING! Failed to replenish keypool, please unlock your wallet to do so.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Last PrivateSend message:
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>PrivateSend was successfully reset.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>If you don&apos;t want to see internal PrivateSend fees/transactions select &quot;Most Common&quot; as Type on the &quot;Transactions&quot; tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>PrivateSend requires at least %1 to use.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet is locked and user declined to unlock. Disabling PrivateSend.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+327"/>
        <location line="+216"/>
        <location line="+42"/>
        <location line="+113"/>
        <location line="+14"/>
        <location line="+18"/>
        <source>Payment request error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-402"/>
        <source>Cannot start ion: click-to-pay handler</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+103"/>
        <location line="+14"/>
        <location line="+7"/>
        <source>URI handling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-20"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid payment address %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>URI cannot be parsed! This can be caused by an invalid Ion address or malformed URI parameters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Payment request file handling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+61"/>
        <location line="+9"/>
        <location line="+31"/>
        <location line="+10"/>
        <location line="+17"/>
        <location line="+88"/>
        <source>Payment request rejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-155"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Payment request expired.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment request is not initialized.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <location line="+17"/>
        <source>Invalid payment request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-10"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+55"/>
        <source>Refund from %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+44"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Error communicating with %1: %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Payment request cannot be parsed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Bad response from server %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Network request error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment acknowledged</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+117"/>
        <source>NodeId</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ion.cpp" line="+176"/>
        <source>%1 didn&apos;t yet exit safely...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../bitcoinunits.cpp" line="+233"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+133"/>
        <source>Enter a Ion address (e.g. %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+837"/>
        <source>%1 d</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <location line="+47"/>
        <source>%1 s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-10"/>
        <source>None</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>%1 ms</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+18"/>
        <source>%n second(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minute(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+6"/>
        <source>%n week(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+0"/>
        <source>%n year(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>%1 and %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="-29"/>
        <source>unknown</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject::QObject</name>
    <message>
        <location filename="../ion.cpp" line="-81"/>
        <source>Error: Specified data directory &quot;%1&quot; does not exist.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: Cannot parse configuration file: %1. Only use key=value syntax.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Error: %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QRDialog</name>
    <message>
        <location filename="../forms/qrdialog.ui" line="+23"/>
        <source>QR-Code Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>QR Code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>&amp;Save Image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../qrdialog.cpp" line="+153"/>
        <source>Error creating QR Code.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QRGeneralImageWidget</name>
    <message>
        <location line="-117"/>
        <source>&amp;Save Image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Save QR Code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Save QR Code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+14"/>
        <source>Tools window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>General</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+144"/>
        <source>Name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-127"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+36"/>
        <location line="+23"/>
        <location line="+20"/>
        <location line="+30"/>
        <location line="+23"/>
        <location line="+36"/>
        <location line="+23"/>
        <location line="+665"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1201"/>
        <source>Number of connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+183"/>
        <source>&amp;Open</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-242"/>
        <source>Startup time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+103"/>
        <source>Last block time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+100"/>
        <source>Debug log file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-123"/>
        <source>Current number of blocks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-210"/>
        <source>Client version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+49"/>
        <source>Using BerkeleyDB version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+154"/>
        <source>Block chain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-20"/>
        <source>Number of Masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+79"/>
        <source>Memory Pool</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Current number of transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Memory usage</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+77"/>
        <source>&amp;Console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+100"/>
        <source>Clear console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+95"/>
        <source>&amp;Network Traffic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+64"/>
        <location line="+560"/>
        <source>Received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-480"/>
        <location line="+457"/>
        <source>Sent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-416"/>
        <source>&amp;Peers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+53"/>
        <source>Banned peers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+62"/>
        <location filename="../rpcconsole.cpp" line="+484"/>
        <location line="+795"/>
        <source>Select a peer to view detailed information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Whitelisted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+69"/>
        <source>Starting Block</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+534"/>
        <source>Wallet Path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1629"/>
        <location line="+1003"/>
        <source>User Agent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-951"/>
        <source>Datadir</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+265"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+68"/>
        <source>Decrease font size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Increase font size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+612"/>
        <source>Services</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+92"/>
        <source>Ban Score</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Min Ping</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>&amp;Wallet Repair</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Salvage wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Rescan blockchain files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Recover transactions 1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Recover transactions 2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Upgrade wallet format</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>The buttons below will restart the wallet with command-line options to repair the wallet, fix issues with corrupt blockhain files or missing/obsolete transactions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>-salvagewallet: Attempt to recover private keys from a corrupt wallet.dat.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>-rescan: Rescan the block chain for missing wallet transactions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>-zapwallettxes=1: Recover transactions from blockchain (keep meta-data, e.g. account owner).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>-zapwallettxes=2: Recover transactions from blockchain (drop meta-data).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>-upgradewallet: Upgrade wallet to latest format on startup. (Note: this is NOT an update of the wallet itself!)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Wallet repair options.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Rebuild index</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>-reindex: Rebuild block chain index from current blk000??.dat files.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-699"/>
        <source>&amp;Disconnect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban for</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+188"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>WARNING: Scammers have been active, telling users to type commands here, stealing their wallet contents. Do not use this console without fully understanding the ramification of a command.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>In:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Network activity disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>Total: %1 (Enabled: %2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+127"/>
        <source>%1 B</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+99"/>
        <source>(node id: %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>never</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Inbound</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>हाँ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>नहीं</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+34"/>
        <source>Reuse one of the previously used receiving addresses.&lt;br&gt;Reusing addresses has security and privacy issues.&lt;br&gt;Do not use this unless re-generating a payment request made before.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Ion network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Message:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <location line="+21"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-14"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened.&lt;br&gt;Note: The message will not be sent with the payment over the Ion network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <location line="+22"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-19"/>
        <source>&amp;Amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>&amp;Request payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear all fields of the form.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Request InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+49"/>
        <source>Requested payments history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Remove the selected entries from the list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+48"/>
        <source>Copy URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR कोड</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+65"/>
        <source>Request payment to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>पता</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Label</source>
        <translation>लेबल</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>संदेश</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Error encoding URI into QR Code.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+29"/>
        <source>Date</source>
        <translation>मिति</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>लेबल</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>संदेश</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>(no label)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount requested)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+42"/>
        <source>Requested</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+665"/>
        <source>Send Coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Inputs...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>मात्रा:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>राशि:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Fee:</source>
        <translation>फ़ीस:</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Dust:</source>
        <translation>धूल:</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>After Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom change address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+206"/>
        <source>Transaction Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>Using the fallbackfee can result in sending a transaction that will take several hours or days (or never) to confirm. Consider choosing your fee manually or wait until your have validated the complete chain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Warning: Fee estimation is currently not possible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>collapse fee-settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+256"/>
        <source>Confirmation time target:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+231"/>
        <source>PrivateSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-455"/>
        <source>If the custom fee is set to 1000 duffs and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 duffs in fee,&lt;br /&gt;while &quot;at least&quot; pays 1000 duffs. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 duffs and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 duffs in fee,&lt;br /&gt;while &quot;total at least&quot; pays 1000 duffs. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks.&lt;br /&gt;But be aware that this can end up in a never confirming transaction once there is more demand for ion transactions than the network can process.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-59"/>
        <source>per kilobyte</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-51"/>
        <source>Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>(read the tooltip)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Recommended:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Custom:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>normal</source>
        <translation>साधारण</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>fast</source>
        <translation>तेज</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Confirm the send action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Clear all fields of the form.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear &amp;All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Send to multiple recipients at once</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+60"/>
        <source>Balance:</source>
        <translation>शेष राशि:</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-567"/>
        <source>Copy quantity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+153"/>
        <location line="+4"/>
        <location line="+8"/>
        <source>using</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-12"/>
        <location line="+4"/>
        <source>anonymous funds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>(privatesend requires this amount to be rounded up to the nearest %1).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>any available funds (not anonymous)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>and InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+76"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Are you sure you want to send?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>are added as transaction fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Total Amount = &lt;b&gt;%1&lt;/b&gt;&lt;br /&gt;= %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>&lt;b&gt;(%1 of %2 entries displayed)&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirm send coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+212"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected with the following reason: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Payment request expired.</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+67"/>
        <source>%n block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Pay only the required fee of %1</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+30"/>
        <source>Estimated to begin confirmation within %n block(s).</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+103"/>
        <source>Warning: Invalid Ion address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: Unknown change address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirm custom change address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>(no label)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+21"/>
        <source>This is a normal payment.</source>
        <translation>यह एक सामान्य भुगतान है |</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Pay &amp;To:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>The Ion address to send the payment to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Alt+P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+544"/>
        <location line="+529"/>
        <source>Remove this entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1055"/>
        <source>&amp;Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Enter a label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+535"/>
        <location line="+529"/>
        <source>A&amp;mount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1046"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive a lower amount of Ion than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Message:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>A message that was attached to the ion: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Ion network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+433"/>
        <source>This is an unauthenticated payment request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+525"/>
        <source>This is an authenticated payment request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-510"/>
        <location line="+525"/>
        <source>Pay To:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-495"/>
        <location line="+529"/>
        <source>Memo:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="+32"/>
        <source>Enter a label for this address to add it to your address book</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SendConfirmationDialog</name>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="+97"/>
        <location line="+5"/>
        <source>Yes</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+71"/>
        <source>%1 is shutting down...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive Ion sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>The Ion address to sign the message with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+190"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-184"/>
        <location line="+190"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-180"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Alt+P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>हस्ताक्षर</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Sign the message to prove you own this Ion address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Reset all sign message fields</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+131"/>
        <source>Clear &amp;All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-76"/>
        <source>&amp;Verify Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>The Ion address the message was signed with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>Verify the message to ensure it was signed with the specified Ion address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Reset all verify message fields</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+33"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+104"/>
        <location line="+80"/>
        <source>The entered address is invalid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-80"/>
        <location line="+8"/>
        <location line="+72"/>
        <location line="+8"/>
        <source>Please check the address and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-80"/>
        <location line="+80"/>
        <source>The entered address does not refer to a key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-72"/>
        <source>Wallet unlock was cancelled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Private key for the entered address is not available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Message signing failed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Message signed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+58"/>
        <source>The signature could not be decoded.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+13"/>
        <source>Please check the signature and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>The signature did not match the message digest.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Message verification failed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Message verified.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+24"/>
        <source>[testnet]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../splashscreen.cpp" line="+50"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+94"/>
        <source>KB/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message numerus="yes">
        <location filename="../transactiondesc.cpp" line="+33"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open until %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>conflicted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>%1/offline</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>0/unconfirmed, %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>in memory pool</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>not in memory pool</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>abandoned</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>locked via LLMQ based ChainLocks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>verified via LLMQ based InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>verified via InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>InstantSend verification in progress - %1 of %2 signatures</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>InstantSend verification failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>, has not been successfully broadcast yet</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <source>, broadcast through %n node(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Date</source>
        <translation>मिति</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <location line="+13"/>
        <location line="+72"/>
        <source>From</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-72"/>
        <source>unknown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+20"/>
        <location line="+69"/>
        <source>To</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-87"/>
        <source>own address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+69"/>
        <source>watch-only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-67"/>
        <source>label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <location line="+12"/>
        <location line="+53"/>
        <location line="+26"/>
        <location line="+55"/>
        <source>Credit</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="-144"/>
        <source>matures in %n more block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+59"/>
        <location line="+25"/>
        <location line="+55"/>
        <source>Debit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-70"/>
        <source>Total debit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Total credit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Transaction fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Net amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <location line="+11"/>
        <source>Message</source>
        <translation>संदेश</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Output index</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction total size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Merchant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Debug information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <source>true</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1"/>
        <location line="+1"/>
        <source>false</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+20"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../transactiondescdialog.cpp" line="+20"/>
        <source>Details for %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+247"/>
        <source>Date</source>
        <translation>मिति</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Address / Label</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+58"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+39"/>
        <source>Received with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Received via PrivateSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>PrivateSend Denominate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>PrivateSend Collateral Payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>PrivateSend Make Collateral Inputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>PrivateSend Create Denominations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>PrivateSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>watch-only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>(n/a)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+240"/>
        <source>(no label)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+39"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not this transaction was locked by InstantSend.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+70"/>
        <location line="+11"/>
        <location line="+17"/>
        <source>All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-27"/>
        <source>Locked by InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Not locked by InstantSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Today</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Most Common</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Received with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>PrivateSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend Make Collateral Inputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend Create Denominations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend Denominate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend Collateral Payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>To yourself</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter address or label to search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Min amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Abandon transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy full transaction details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show address QR code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+223"/>
        <source>Export Transaction History</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Comma separated file (*.csv)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>मिति</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>लेबल</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>पता</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Exporting Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Successful</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+143"/>
        <source>QR code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+43"/>
        <source>Range:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>to</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../bitcoingui.cpp" line="+138"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+27"/>
        <source>No wallet has been loaded.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+300"/>
        <location line="+31"/>
        <location line="+5"/>
        <location line="+11"/>
        <source>Send Coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-47"/>
        <location line="+31"/>
        <source>InstantSend doesn&apos;t support sending values that high yet. Transactions are currently limited to %1 ION.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Used way too many inputs (&gt;%1) for this InstantSend transaction, fees could be huge.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+49"/>
        <source>&amp;Export</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Export the data in the current tab to a file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Selected amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+240"/>
        <source>Backup Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet Data (*.dat)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Backup Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Backup Successful</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ion-core</name>
    <message>
        <location filename="../ionstrings.cpp" line="+36"/>
        <source>Bind to given address and always listen on it. Use [host]:port notation for IPv6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+54"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Execute command when a relevant alert is received or we see a really long fork (%s in cmd is replaced by message)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Execute command when a wallet transaction changes (%s in cmd is replaced by TxID)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Execute command when the best block changes (%s in cmd is replaced by block hash)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+74"/>
        <source>Name to construct url for KeePass entry that stores the wallet passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+51"/>
        <source>Set maximum size of high-priority/low-fee transactions in bytes (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the number of script verification threads (%u to %d, 0 = auto, &lt;0 = leave that many cores free, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+65"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Accept command line and JSON-RPC commands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Add a node to connect to and attempt to keep the connection open</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Allow DNS lookups for -addnode, -seednode and -connect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Already have that input.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Block creation options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot downgrade wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot write default address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Collateral not valid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Connect to a node to retrieve peer addresses, and disconnect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Connection options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Corrupted block database detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Debugging/Testing options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not load the wallet and disable wallet RPC calls</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Done loading</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Entries are full.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading block database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error opening block database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error reading from database, shutting down.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: Disk space is low!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-391"/>
        <source>(1 = keep tx meta data e.g. account owner and payment request information, 2 = drop tx meta data)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>-maxtxfee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>A fee rate (in %s/kB) that will be used when fee estimation has insufficient data (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Accept relayed transactions received from whitelisted peers even when not relaying transactions (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow JSON-RPC connections from specified source. Valid for &lt;ip&gt; are a single IP (e.g. 1.2.3.4), a network/netmask (e.g. 1.2.3.4/255.255.255.0) or a network/CIDR (e.g. 1.2.3.4/24). This option can be specified multiple times</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Bind to given address and whitelist peers connecting to it. Use [host]:port notation for IPv6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Create new files with system default permissions, instead of umask 077 (only effective with disabled wallet functionality)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete all wallet transactions and only recover those parts of the blockchain through -rescan on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Enable publish raw transactions of attempted InstantSend double spend in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable publish transaction hashes of attempted InstantSend double spend in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Found unconfirmed denominated outputs, will wait till they confirm to continue.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>How thorough the block verification of -checkblocks is (0-4, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>If paytxfee is not set, include enough fee so transactions begin confirmation on average within n blocks (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>If this block is in the chain assume that it and its ancestors are valid and potentially skip their script verification (0 to verify all, default: %s, testnet: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid amount for -maxtxfee=&lt;amount&gt;: &apos;%s&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Maintain a full transaction index, used by the getrawtransaction rpc call (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Make sure to encrypt your wallet and delete all non-encrypted backups after you verified that wallet works!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Maximum size of data in data carrier transactions we relay and mine (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Number of seconds to keep misbehaving peers from reconnecting (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Output debugging information (default: %u, supplying &lt;category&gt; is optional)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Overrides minimum spork signers to change spork value. Only useful for regtest and devnet. Using this on mainnet or testnet will ban you.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+90"/>
        <source>Use N separate masternodes in parallel to mix funds (%u-%u, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>User defined mnemonic for HD wallet (bip39). Only has effect during wallet creation/first start (default: randomly generated)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>User defined seed for HD wallet (should be in hex). Only has effect during wallet creation/first start (default: randomly generated)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>Whitelisted peers cannot be DoS banned and their transactions are always relayed, even if they are already in the mempool, useful e.g. for a gateway</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>You need to rebuild the database using -reindex-chainstate to change -txindex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>You should specify a masternodeblsprivkey in the configuration. Please see documentation for help.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>(default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>-wallet parameter must only specify a filename (not a path)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Accept public REST requests (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Always query for peer addresses via DNS lookup (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Can&apos;t mix: no compatible inputs found!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Chain selection options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Connect through SOCKS5 proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Connect to KeePassHttp on port &lt;port&gt; (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Enable the client to act as a masternode (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry exceeds maximum size.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Failed to load fulfilled requests cache from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to load governance cache from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to load masternode cache from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Found enough users, signing ( waiting %s )</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Found enough users, signing ...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>How many blocks to check at startup (default: %u, 0 = all)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Include IP addresses in debug output (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Incompatible mode.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Incompatible version.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Information</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Input is not valid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Insufficient funds.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid amount for -paytxfee=&lt;amount&gt;: &apos;%s&apos; (must be at least %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid masternodeblsprivkey. Please see documenation.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid minimum number of spork signers specified with -minsporkkeys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Listen for JSON-RPC connections on &lt;port&gt; (default: %u or testnet: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Listen for connections on &lt;port&gt; (default: %u or testnet: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Loading banlist...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading fulfilled requests cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading masternode cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Lock is already in place.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Maximum per-connection receive buffer, &lt;n&gt;*1000 bytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximum per-connection send buffer, &lt;n&gt;*1000 bytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Mixing in progress...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Mnemonic passphrase is too long, must be at most 256 characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>No Masternodes detected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>No compatible Masternode found.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Not in the Masternode list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Only connect to nodes in network &lt;net&gt; (ipv4, ipv6 or onion)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Prepend debug output with timestamp (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Rebuild chain state and block index from the blk*.dat files on disk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Rebuild chain state from the currently indexed blocks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Send trace/debug info to debug.log file (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Send transactions as zero-fee transactions if possible (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Set key pool size to &lt;n&gt; (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the masternode BLS private key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set the number of threads to service RPC calls (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Specify configuration file (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify connection timeout in milliseconds (minimum: 1, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Specify pid file (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Spend unconfirmed change when sending transactions (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Submitted to masternode, waiting in queue %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronization failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronization finished</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>This is not a Masternode.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Threshold for disconnecting misbehaving peers (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Too many %f denominations, removing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Too many %f denominations, skipping.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Tor control port password (default: empty)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Tor control port to use if onion listening enabled (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to start HTTP server. See debug log for details.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown response.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported argument -benchmark ignored, use -debug=bench.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unsupported argument -debugnet ignored, use -debug=net.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unsupported argument -tor found, use -onion.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrade wallet to latest format on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Use KeePass 2 integration using KeePassHttp plugin (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Use UPnP to map the listening port (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Use the test chain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Will retry...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-231"/>
        <source>Can&apos;t find random Masternode.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Can&apos;t mix while sync in progress.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid script detected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>KeePassHttp id for the established association</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>KeePassHttp key for AES encrypted communication with KeePass</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Keep at most &lt;n&gt; unconnectable transactions in memory (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-385"/>
        <source>Disable all Ion specific functionality (Masternodes, PrivateSend, InstantSend, Governance) (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-45"/>
        <source>%s file contains all private keys from this wallet. Do not share it with anyone!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>Bind to given address to listen for JSON-RPC connections. This option is ignored unless -rpcallowip is also passed. Port is optional and overrides -rpcport. Use [host]:port notation for IPv6. This option can be specified multiple times (default: 127.0.0.1 and ::1 i.e., localhost, or if -rpcallowip has been specified, 0.0.0.0 and :: i.e., all addresses)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Discover own IP addresses (default: 1 when listening and no -externalip or -proxy)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Do not keep transactions in the mempool longer than &lt;n&gt; hours (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable InstantSend, show confirmations for locked transactions (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable multiple PrivateSend mixing sessions per block, experimental (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Enable use of automated PrivateSend for funds stored in this wallet (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Execute command when a wallet InstantSend transaction is successfully locked (%s in cmd is replaced by TxID)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Extra transactions to keep in memory for compact block reconstructions (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to create backup, file already exists! This could happen if you restarted wallet in less than 60 seconds. You can continue if you are ok with this.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Fees (in %s/kB) smaller than this are considered zero fee for relaying, mining and transaction creation (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Fees (in %s/kB) smaller than this are considered zero fee for transaction creation (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>If &lt;category&gt; is not supplied or if &lt;category&gt; = 1, output all debugging information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>InstantSend doesn&apos;t support sending values that high yet. Transactions are currently limited to %1 ION.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>InstantSend requires inputs with at least %d confirmations, you might need to wait a few minutes and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Maintain a full address index, used to query for the balance, txids and unspent outputs for addresses (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Maintain a full spent index, used to query the spending txid and input index for an outpoint (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Maintain a timestamp index for block hashes, used to query blocks hashes by a range of timestamps (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Maintain at most &lt;n&gt; connections to peers (temporary service connections excluded) (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>PrivateSend uses exact denominated amounts to send funds, you might simply need to anonymize some more coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Randomize credentials for every proxy connection. This enables Tor stream isolation (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Specify full path to directory for automatic wallet backups (must exist)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Support filtering of blocks and transaction with bloom filters (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Total length of network version string (%i) exceeds maximum length (%i). Reduce the number or size of uacomments.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction index can&apos;t be disabled in full mode. Either start with -litemode command line switch or enable transaction index.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Tries to keep outbound traffic under the given target (in MiB per 24h), 0 = no limit (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to locate enough PrivateSend denominated funds for this transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Unsupported argument -socks found. Setting SOCKS version isn&apos;t possible anymore, only SOCKS5 proxies are supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported argument -whitelistalwaysrelay ignored, use -whitelistrelay and/or -whitelistforcerelay.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Use UPnP to map the listening port (default: 1 when listening and no -proxy)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>WARNING! Failed to replenish keypool, please unlock your wallet to do so.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Wallet is locked, can&apos;t replenish keypool! Automatic backups and mixing are disabled, please unlock your wallet to replenish keypool.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>You are starting in lite mode, all Ion-specific functionality is disabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>(default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>-maxmempool must be at least %d MB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;category&gt; can be:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Append comment to the user agent string</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic backups disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatically create Tor hidden service (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>ERROR! Failed to create automatic backup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish hash block in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish hash of governance objects (like proposals) in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish hash of governance votes in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish hash transaction (locked via InstantSend) in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish hash transaction in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish raw block in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish raw transaction (locked via InstantSend) in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish raw transaction in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to create backup %s!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to create backup, error: %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to delete backup, error: %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to load InstantSend data cache from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Failed to load sporks cache from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee (in %s/kB) to add to transactions you send (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Imports blocks from external blk000??.dat file on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>InstantSend options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid amount for -fallbackfee=&lt;amount&gt;: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Keep the transaction memory pool below &lt;n&gt; megabytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Last PrivateSend was too recent.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Last successful PrivateSend action was too recent.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Loading InstantSend data cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Loading block index...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading governance cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading sporks cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet... (%3.2f %%)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Location of the auth cookie (default: data dir)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Make the wallet broadcast transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Masternode options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Masternode queue is full.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Masternode:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Minimum bytes per sigop in transactions we relay and mine (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Missing input transaction information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>No errors detected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>No matching denominations found for mixing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Node relay options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Non-standard public key detected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Not compatible with existing transactions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Not enough file descriptors available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Not enough funds to anonymize.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Number of automatic wallet backups (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Password for JSON-RPC connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Print version and exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend is idle.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend request complete:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>PrivateSend request incomplete:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Pruning blockstore...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Rescan the block chain for missing wallet transactions on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>Submitted following entries to masternode: %u</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Submitted to masternode, waiting for more entries ( %u ) %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Synchronizing blockchain...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>The wallet will avoid paying less than the minimum relay fee.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>This is the minimum transaction fee you pay on every transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This is the transaction fee you will pay if you send a transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Transaction amounts must not be negative</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has too long of a mempool chain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction must have at least one recipient</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction too large</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Trying to connect...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Upgrading UTXO database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Use devnet chain with provided name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Wallet debugging/testing options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Wallet is not initialized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning: unknown new rules activated (versionbit %i)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wasn&apos;t able to create wallet backup folder %s!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Whether to operate in a blocks only mode (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>You can not start a masternode in lite mode.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>ZeroMQ notification options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>no mixing available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>see debug.log for details.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-100"/>
        <source>RPC server options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-486"/>
        <source>Ion Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Accept connections from outside (default: 1 if no -proxy or -connect/-noconnect)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Connect only to the specified node(s); -noconnect or -connect=0 alone to disable automatic connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>Distributed under the MIT software license, see the accompanying file %s or %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Force relay of transactions from whitelisted peers even if they violate local relay policy (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Incorrect or no devnet genesis block found. Wrong datadir for devnet specified?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Maximum allowed median peer time offset adjustment. Local perspective of time may be influenced by peers forward or backward by this amount. (default: %u seconds)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Maximum total fees (in %s) to use in a single wallet transaction or raw transaction; setting this too low may abort large transactions (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Override spork address. Only useful for regtest and devnet. Using this on mainnet or testnet will ban you.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Please contribute if you find %s useful. Visit %s for further information about the software.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Provide liquidity to PrivateSend by infrequently mixing coins on a continual basis (%u-%u, default: %u, 1=very frequent, high fees, %u=very infrequent, low fees)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Query for peer addresses via DNS lookup, if low on addresses (default: 1 unless -connect/-noconnect)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Reduce storage requirements by enabling pruning (deleting) of old blocks. This allows the pruneblockchain RPC to be called to delete specific blocks, and enables automatic pruning of old blocks if a target size in MiB is provided. This mode is incompatible with -txindex and -rescan. Warning: Reverting this setting requires re-downloading the entire blockchain. (default: 0 = disable pruning blocks, 1 = allow manual pruning via RPC, &gt;%u = automatically prune block files to stay under the specified target size in MiB)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Set lowest fee rate (in %s/kB) for transactions to be included in block creation. (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>This is the transaction fee you may pay when fee estimates are not available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit %s and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to locate enough PrivateSend non-denominated funds for this transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Use N separate masternodes for each denominated input to mix funds (%u-%u, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Use hierarchical deterministic key generation (HD) after BIP39/BIP44. Only has effect during wallet creation/first start</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>User defined mnemonic passphrase for HD wallet (BIP39). Only has effect during wallet creation/first start (default: empty string)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Username and hashed password for JSON-RPC connections. The field &lt;userpw&gt; comes in the format: &lt;USERNAME&gt;:&lt;SALT&gt;$&lt;HASH&gt;. A canonical python script is included in share/rpcuser. The client then connects normally using the rpcuser=&lt;USERNAME&gt;/rpcpassword=&lt;PASSWORD&gt; pair of arguments. This option can be specified multiple times</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Wallet will not create transactions that violate mempool chain limits (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Whitelist peers connecting from the given IP address (e.g. 1.2.3.4) or CIDR notated network (e.g. 1.2.3.0/24). Can be specified multiple times.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>%s corrupt, salvage failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>%s is not a valid backup folder!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>%s is set very high!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>(press q to shutdown and continue later)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>-devnet can only be specified once</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>-port must be specified when -devnet and -listen are specified</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>-rpcport must be specified when -devnet and -server are specified</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Allow RFC1918 addresses to be relayed and connected to (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Attempt to recover private keys from a corrupt wallet on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot resolve -%s address: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Change index out of range</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Copyright (C)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Error loading %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Error upgrading chainstate database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to find mixing queue to join</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to start a new mixing queue</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid -onion address or hostname: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -proxy address or hostname: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -%s=&lt;amount&gt;: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid characters in -wallet filename</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid spork address specified with -sporkaddr</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Keep N ION anonymized (%u-%u, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Loading P2P addresses...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Print this help message and exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Receive and display P2P network alerts (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Reducing -maxconnections from %d to %d, because of system limitations.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Relay and mine data carrier transactions (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Relay non-P2SH multisig (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Rescanning...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Run in the background as a daemon and accept commands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Send trace/debug info to console instead of debug.log file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Session not complete!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Session timed out.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set database cache size in megabytes (%d to %d, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Set maximum block size in bytes (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show all debugging options (usage: --help -help-debug)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Shrink debug.log file on client startup (default: 1 when no -debug)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing transaction failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Specify data directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Specify wallet file (within data directory)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify your own public address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Starting network threads...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Synchronization pending...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Synchronizing governance objects...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The source code is available from %s.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>This is experimental software.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Transaction amount too small</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction created successfully.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction fees are too high.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction not valid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to sign spork message, wrong key?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown state: id = %u</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Username for JSON-RPC connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Very low number of keys left: %d</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Wallet is locked.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Wallet options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet window title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning</source>
        <translation>चेतावनी</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Your entries added successfully.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Your transaction was accepted into the pool!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Zapping all transactions from wallet...</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>